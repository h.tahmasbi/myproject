
// 1. Variables
let name = 'Hossein';
console.log(name);
// Rules for naming the variables:
// Cannot be reserved keyword (if, less, etc)
// Should be meaningful (x, y, etc)
// Cannot start with a number (1name)
// Cannot contain a space or hyphen (-)
// Are case-sensitive (Camel notation)

let firstName = 'Hossein';
let lastName = 'Tahmasbi';

console.log(firstName, lastName);

// 2. Constants
// define constants when you want not to be changed

const interestRate = 0.3;
// interestRate = 1;
console.log(interestRate);

// 3. Primitive / Value Types  
let name = 'Hossein'; // String Literal
let age = 30; // Number Literal
let isApproved = false; // Boolean Literal
let firstName = undefined; // or firstName; for undefined
let selectedColor = null; // to clear the value of a variable

//