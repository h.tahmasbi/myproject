# comparison operators
# >, <, >= , <=, ==, !=
"bag" == "Bag"  # False
ord("b")  # 98
ord("B")  # 66
# -----------------------------------------
# conditional statements
temperature = 15
if temperature > 30:
    print("It's warm")
    print("Drink water")
elif temperature > 20:
    print("It's nice")
else:
    print("It's cold")
print("Done")

# -----------------------------------------
# Ternary operator
age = 22
# if age >= 18:
#    #print("Eligible")
#    message = "Eligible" 
# else:
#    #print("Not eligible")
#    message = "Not eligible"

message = "Eligible" if age >= 18 else "Not eligible"
print(message)

# -----------------------------------------
# logical operators : and, or, not
high_income = False
good_credit = True
student = True

# if high_income and good_credit:
# if high_income or good_credit:
# if not student:
if (high_income or good_credit) and not student:
    print("Eligible")
else:
    print("Not eligible")
# short-circuit Evaluation
# if X or Y: (Y is executed only if X is False) 
if high_income or good_credit or not student:
    print("Eligible")
# -----------------------------------------
# Chaining comparison operators
# age should be between 18 and 65
age = 22
# if age >= 18 and age < 65:
if 18 <= age < 65:
    print("Eligible")

# -----------------------------------------
# for loops
# range(3), range(1,3), range(1,10,2)
for number in range(1, 10, 2):
    print("Attempt", number, number * ".")

# -----------------------------------------
# for..Else
successful = True
for number in range(3):
    print("Attempt")
    if successful:
        print("Successful")
        break
else:
    print("Attempted 3 times and failed")
# -----------------------------------------
# Nested loops
for x in range(5):
    for y in range(4):
        print(f"({x}, {y})")
# -----------------------------------------
# Iterables
type(type(5))
type(type(range(5)))
# Iterable
# range(), strings, lists
for x in "Python":
    print(x)
    # break
# for item in shopping_cart:
for x in [1, 2, 3, 4]:
    print(x)

# -----------------------------------------
# While
number = 100
while number > 0:
    print(number)
    number //= 2

command = ""
# while command != "quit" and command != "QUIT":
while command.lower() != "quit":
    command = input(">")
    print("ECHO", command)

# infinite loops
# command = ""
while True:
    command = input(">")
    print("ECHO", command)
    if command.lower() == "quit":
        break
