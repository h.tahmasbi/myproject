# working with SQLite files
import sqlite3
import json
from pathlib import Path

## read json file
#movies = json.loads(Path("movies.json").read_text())
#print(movies)
#with sqlite3.connect("db.sqlite3") as conn:
#    command = "INSERT INTO Movies VALUES (?, ?, ?)"
#    for movie in movies:
#        conn.execute(command, tuple(movie.values()))
#    # to write all changes into database
#    conn.commit()

# after installing DB Browser for SQLite and create Movies

with sqlite3.connect("db.sqlite3") as conn:
    command = "SELECT * FROM Movies"
    cursor = conn.execute(command)
#    for row in cursor:
#        print(row)
# end of cursor
    movies = cursor.fetchall()
    print(movies)
