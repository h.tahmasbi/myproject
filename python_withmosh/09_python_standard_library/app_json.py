# working with JSON (java script object notation) files -> in websites using JSON is more popular.

import json
from pathlib import Path

# write to file as json format
#movies = [
#        {"id": 1, "title": "Terminator", "year": 1989},
#        {"id": 2, "title": "Kindergarten Cop", "year": 1993}
#]

#data = json.dumps(movies)
#print(data)
#Path("movies.json").write_text(data)

# read from json file
data = Path("movies.json").read_text()
movies = json.loads(data)
print(movies)

