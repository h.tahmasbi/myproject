# working with directories

from pathlib import Path

path = Path("../08_Modules/ecommerce")

#path.exists()
#path.mkdir()
#path.rmdir()
#path.rename("ecommerce2")

# iterdir() create a generator -> generator return new value every time iterative -> efficient using of memory 
print(path.iterdir())

# if we have a directory with milloins of files we use a for loop
for p in path.iterdir():
    print(p)

# else we use comprehension expression
paths = [p for p in path.iterdir() if p.is_dir()]
print(paths)

# iterdir() is pretty useful but has two limitations
# 1. we can not search by pattern 2. it does not search recursively
# so different method that is glob() should be used.
# rglob() or .glop("**/*.py"): recursively -> all files and directoris in the path and all its childeren
py_files = [p for p in path.rglob("*.py")]
print(py_files)
