# working with paths
# https://docs.python.org/3/library/pathlib.html

from pathlib import Path

#Path("/usr/local/bin")
## current folder
#Path()
#Path("ecommerce/__init__.py")
## combine the addresses or strings
#Path() / Path("ecommerce/__init__.py")
#Path() / "ecommerce" / "__init__.py"
#Path.home()
path = Path("ecommerce/__init__.py")
path.exists()
path.is_file()
path.is_dir()
print(path.name)
print(path.stem)
print(path.suffix)
print(path.parent)
# to creat a new path object based on the current existing path 
# to change the name or suffix -> note that by this method we have not changed the name or suffix
#path = path.with_name("file.txt")
path = path.with_suffix(".txt")
print(path)
print(path.absolute())
