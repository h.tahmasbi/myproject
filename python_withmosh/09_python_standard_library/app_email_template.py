# Sending emails -> using a html template for body
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
# mime: multi-purpose internet mail extension -> a standard to define email
from pathlib import Path
# to replace $name parameter in template.html file:
from string import Template
import smtplib

template = Template(Path("template.html").read_text())
message = MIMEMultipart()
message["from"] = "Hossein Tahmasbi"
message["to"] = "h.tahmasb@gmail.com"
message["subject"] = "This is a test"
body = template.substitute({"name" : "John"})
# or
# body = template.substitute(name="John")
message.attach(MIMEText(body, "html"))
# to attach picture
message.attach(MIMEImage(Path("figure.png").read_bytes()))

with smtplib.SMTP(host="smtp.gmail.com", port="587") as smtp:
    smtp.ehlo()
    #tls: transport layer security -> to encrypt
    smtp.starttls()
    smtp.login("h.tahmasb@gmail.com", "password")
    smtp.send_message(message)
    print("Sent...")
