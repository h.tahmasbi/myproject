# working with datetime -> point time
# working with timedelta -> duration

from datetime import datetime, timedelta
import time

dt1 = datetime(2018, 1, 1) + timedelta(days=1, seconds=1000)
print(dt1)
dt2 = datetime.now()
dt3 = datetime.strptime("2018/01/12", "%Y/%m/%d") # to convert string to datetime object
dt = datetime.fromtimestamp(time.time())  # to convert current time to datetime object

print(f"{dt.year}/{dt.month}")

print(dt.strftime("%Y/%m"))   # to convert a datetime object to a string != strptime
print(dt2 > dt1)

duration = dt2 -dt1
print(duration)
print("days", duration.days)
print("seconds", duration.days)
print("total_seconds", duration.total_seconds())
