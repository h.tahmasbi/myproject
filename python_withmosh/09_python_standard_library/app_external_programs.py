# Running external programs from a python script
import subprocess

#completed = subprocess.run(["ls", "-l"],
try:
    #completed = subprocess.run(["python3", "other.py"],
    completed = subprocess.run(["False"],
                                capture_output=True,
                                text=True,
                                check=True) # to check there is an error or not
    #print(type(completed))
    print("args", completed.args)
    print("returncode", completed.returncode)
    print("stderr", completed.stderr)
    print("stdout", completed.stdout)
except subprocess.CalledProcessError as ex:
    print(ex)

