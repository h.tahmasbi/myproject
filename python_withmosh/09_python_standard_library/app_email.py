# Sending emails
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
# mime: multi-purpose internet mail extension -> a standard to define email
from pathlib import Path
import smtplib

message = MIMEMultipart()
message["from"] = "Hossein Tahmasbi"
message["to"] = "h.tahmasb@gmail.com"
message["subject"] = "This is a test"
message.attach(MIMEText("Body"))
# to attach picture
message.attach(MIMEImage(Path("figure.png").read_bytes()))

with smtplib.SMTP(host="smtp.gmail.com", port="587") as smtp:
    smtp.ehlo()
    #tls: transport layer security -> to encrypt
    smtp.starttls()
    smtp.login("h.tahmasb@gmail.com", "password")
    smtp.send_message(message)
    print("Sent...")
