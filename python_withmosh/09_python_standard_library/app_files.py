# working with files

from pathlib import Path
from time import ctime
import shutil

path = Path("ecommerce/__init__.py")
# path.exit()
# path.rename("init.txt")
# path.unlink() 
print(path.stat())
# to write the time of creation
print(ctime(path.stat().st_ctime))

#path.read_bytes()

# to read contents of the file as string
#print(path.read_text())
#path.write_text("...")
#path.write_bytes()

# to copy a file using shutil is cleaner than using read and write
source = Path("ecommerce/__init__.py")
target = Path() / "__init__.py"

shutil.copy(source, target) # is easier, cleaner
# target.write_text(source.read_text())  # is tedious : too long
