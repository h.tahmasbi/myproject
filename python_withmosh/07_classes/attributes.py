# Class vs Instance Attributes
class Point:
    # class level attributes
    default_color = "red"

    # constructor is a magic method
    def __init__(self, x, y):
        # instance attributes
        self.x = x
        self.y = y

    def draw(self):
        print(f"Point ({self.x}, {self.y})")


Point.default_color = "yellow"
point = Point(1, 2)
print(point.default_color)
print(Point.default_color)
# objects in python are dynamic: we can define new object out of constructor
point.z = 10
point.draw()

another = Point(3, 4)
another.draw()
print(another.default_color)

# Note: Class level attributes are shared across all instances in a class
