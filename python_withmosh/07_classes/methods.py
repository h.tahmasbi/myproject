# Class vs Instance Methods 
class Point:
    # instance method
    def __init__(self, x, y):
        # instance attributes
        self.x = x
        self.y = y

    # decorator
    @classmethod
    def zero(cls):
        return cls(0, 0)

    # instance method
    def draw(self):
        print(f"Point ({self.x}, {self.y})")


# point = Point(0, 0)
# when you want use such this in several points in program 
# factory method
point = Point.zero()

# we can call the instance methods as a instance of the Point class using an object
point.draw()
