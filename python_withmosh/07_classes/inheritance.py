# Inheritance
# A concept in programming called DRY: Don't Repeat Yourself

class Animal:
    # class Animal(object):
    def __init__(self):
        self.age = 1

    def eat(self):
        print("eat")


# Animal: Parent, Base
# Mammal: Child, Subclass
class Mammal(Animal):
    def walk(self):
        print("walk")


class Fish(Animal):
    def swim(self):
        print("swim")


m = Mammal()
m.eat()
print(m.age)
# m is an instance of Mammal class
print(isinstance(m, Mammal))
print(isinstance(m, Animal))

# the object class
# it is a based class for all classes in the python
print(isinstance(m, object))
print(issubclass(Mammal, object))
