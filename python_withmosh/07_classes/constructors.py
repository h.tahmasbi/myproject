# Constructors
# all methods in a class should have a parameter called "self"!
class Point:
    # constructor is a magic method
    def __init__(self, x, y):
        # attributes
        self.x = x
        self.y = y

    def draw(self):
        print(f"Point ({self.x}, {self.y})")


point = Point(1, 2)
point.draw()
