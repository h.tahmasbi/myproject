# Multiple inheritance
class Employee:
    def greet(self):
        print("Employee Greet")


class Person:
    def greet(self):
        print("Person Greet")

class Manager(Employee, Person):
    pass

manager = Manager()
manager.greet()
# In multiple inheritance if there are common functins, then output depends on which class is called first. Here:
# Employee Greet
# Note: when there are common functions, using the multiple inheritance is not good!

# A good example of multiple inheritance
class Flyer:
    def fly(self):
        pass

class Swimmer:
    def swim(self):
        pass

class FlyingFish(Flyer, Swimmer):
    pass
