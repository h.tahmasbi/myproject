# Properties

# To control on an attribute in a class
# The first implementation:
class Product:
    def __init__(self, price):
        # self.__price = price
        self.set_price(price)

    def get_price(self):
        return self.__price

    def set_price(self, value):
        if value < 0:
            raise ValueError("Price cannot be negative")
        self.__price = value


product = Product(50)

# Although the above implementation does work but it is not "pythonic",
# It means the code is ugly and is not using python best practicer
# So there is a better way in Python to get the same results
# Using property


class Product:
    def __init__(self, price):
        # self.__price = price
        # self.set_price(price)
        self.price = price

    # def get_price(self):
    @property
    def price(self):
        return self.__price

    # def set_price(self, value):
    @price.setter
    def price(self, value):
        if value < 0:
            raise ValueError("Price cannot be negative")
        self.__price = value

    # using price property we have a lot of functions, the code is polluted.
    # so we use decorater to convert class method to instance method
    # price = property(get_price, set_price)


product = Product(10)
product.price = 2
print(product.price)
