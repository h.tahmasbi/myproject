# Private members
# Implement a class to keep track of tags (i.g. python) in a blog
class TagCloud:
    # constructor
    def __init__(self):
        # to make tags private or inaccessible use __tags
        self.__tags = {}

    def add(self, tag):
        # syntax: dict.get(key, default_value)
        self.__tags[tag.lower()] = self.__tags.get(tag.lower(), 0) + 1

    def __getitem__(self, tag):
        return self.__tags.get(tag.lower(), 0)

    def __setitem__(self, tag, count):
        self.__tags[tag.lower()] = count

    # to get the number of tags
    def __len__(self):
        return len(self.__tags)

    def __iter__(self):
        iter(self.__tags)


cloud = TagCloud()
# now it is private, so we get AttributeError
# how we can access to it? -> private_members_i.py
print(cloud.__tags)
cloud.add("python")
cloud.add("python")
cloud.add("Python")
print(cloud.__tags["PYTHON"])
