# Magic Methods
# Documentation: https://rszalski.github.io/magicmethods/
class Point:
    # Magic method
    def __init__(self, x, y):
        # instance attributes
        self.x = x
        self.y = y

    # Magic method
    def __str__(self):
        return f"({self.x}, {self.y})"

    def draw(self):
        print(f"Point ({self.x}, {self.y})")


point = Point(1, 2)
print(point)
# print(str(point))
