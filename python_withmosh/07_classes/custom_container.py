# Custom container
# Implement a class to keep track of tags (i.g. python) in a blog
class TagCloud:
    # constructor
    def __init__(self):
        self.tags = {}

    def add(self, tag):
        # syntax: dict.get(key, default_value)
        self.tags[tag.lower()] = self.tags.get(tag.lower(), 0) + 1

    def __getitem__(self, tag):
        return self.tags.get(tag.lower(), 0)

    def __setitem__(self, tag, count):
        self.tags[tag.lower()] = count

    # to get the number of tags
    def __len__(self):
        return len(self.tags)

    def __iter__(self):
        iter(self.tags)


cloud = TagCloud()
cloud["python"] = 10
cloud.add("python")
cloud.add("python")
cloud.add("Python")
print(cloud.tags)
# this code has a tiny problem yet, if :
print(cloud.tags["PYTHON"])
# because all the members are in lowercase
# this problem is fixed in private_members.py
