# Multi-level Inheritance
# Increase the complexity of the software
# Should avoid to use it at all time -> only two level is fine
class Animal:
    def eat(self):
        print("eat")

class Bird(Animal):
    def fly(self):
        print("fly")

class Chicken(Bird):
    pass
