# Method overriding
# We have a constructor in Animal:
class Animal:
    def __init__(self):
        print("Animal Constructor")
        self.age = 1

    def eat(self):
        print("eat")

# If we have also a method constructor in Mammal: 
# __init__
# we got an error, because the constructor in the Animal will not be executed
# Actually this constructor in Mammal (Sub) class replaced the constructor in the base class -> Method overriding
# To fix it we can use the built-in super() function
class Mammal(Animal):
    def __init__(self):
        super().__init__()
        print("Mammal Constructor")
        self.weight = 2
        #super().__init__()

    def walk(self):
        print("walk")

class Fish(Animal):
    def swim(self):
        print("swim")

m = Mammal()
print(m.age)
print(m.weight)
