# Data classes -> 
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

p1 = Point(1, 2)
p2 = Point(1, 2)
print(p1 == p2) 
# False -> different location of memory
print(id(p1))
print(id(p2))

# there is another and better way to implement this code

from collections import namedtuple 

Point = namedtuple("Point", ["x", "y"])

p1 = Point(x=1, y=2)
# we can not change or modify it.
print(p1.x)
#p1.x = 10 #-> error
p1 = Point(x=10, y=2)
print(p1.x)
p2 = Point(x=1, y=2)
print(p1 == p2) 
