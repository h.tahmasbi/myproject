# Class: blueprint for creating new objects
# Object: instance of a class
# create a class
# To name a class we use Pascal naming convention: the first letter of each word is capitalized, No underscore! 
class Point:
    def draw(self):
        print("draw")


point = Point()
print(type(point))
print(isinstance(point, Point))  # point is an instance of the Point class
