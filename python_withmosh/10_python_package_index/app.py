# #
# Pypi: Python package index -> a repository to install python packages
# to install -> pip3
# pip3 list  -> to list all packages installed on a machine 
# 
# pip3 install package==version
# e.g. pip3 install requests==2.9.1
# e.g. pip3 install requests==2.9.*  -> to install the latest compaitable version with 2.9
# or   pip3 install requests~=2.9.0 
# 
# but
# pip3 install requests==2.*   -> latest compaitable version with 2.

import requests

response = requests.get("http://google.com")

print(response)


# virtual environment
# to create an isolated environment to install some packages
# there are two methods to vreate virtual environment
# 1. 
# python3 -m venv env
# to activate it -> source env/bin/activate
# to decativate  -> deactivate

# 2. using "pipenv" it uses pip internally as virtual environment
# then to install packages we use pipenv instead of pip3
