# absolute import
#from ecommerce.customer import contact
# relative import 
#from ..customer import contact

#contact.contact_customer()

print("Sales initialized.", __name__)

def calc_tax():
    pass

def calc_shipping():
    pass

# to make this file usable as a script as well as re-usable as a module
# then one can import it:

# each module has a name that is __main__ if the file is compiled directly.
# if we import this module in another module then this part of code will not be executed. 
# because its name will be changed to ecommerce.shopping.sales
if __name__ == "__main__":
    print("Sales started")
    calc_tax()
