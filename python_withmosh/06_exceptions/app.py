# Exceptions
# index error
# a = [1, 2]
# print(a[2])
# value error
# age = int(input("Age: "))
# How to handle exception errors

try:
    age = int(input("Age: "))
    xfactor = 10 / age
except (ValueError, ZeroDivisionError):  # two exceptions
    # except ValueError:     # one exception
    # except ValueError as ex:
    print("You didn't enter a valid age")
#    print(ex)
#    print(type(ex))
else:
    print("No exceptions were thrown")
# print("Execution continues.")

# Cleaning up
# -> using finally class
try:
    file = open("app.py")
    age = int(input("Age: "))
    xfactor = 10 / age
except (ValueError, ZeroDivisionError):
    print("You didn't enter a valid age")
else:
    print("No exceptions were thrown")
finally:
    file.close()

# With statement : another efficient method to open files

# it does not need close(), since there is a subclass file.__enter__ or file.__exit__ (context management protocol)
# which automatically call the exit method then we do not need close()

try:
    with open("app.py") as file:  # , open("another.txt") as target:
        age = int(input("Age: "))
        xfactor = 10 / age
except (ValueError, ZeroDivisionError):
    print("You didn't enter a valid age")
else:
    print("No exceptions were thrown")
