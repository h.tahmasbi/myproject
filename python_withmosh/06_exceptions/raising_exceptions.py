# Raising Exceptions
# TRY TO NOT RAISING EXCEPTIONS DUE TO ITS COSTING
def calculate_xfactor(age):
    if age <= 0:
        raise ValueError("Age cannot be 0 or less")
    return 10 / age
# google and see built-in exceptions
try:
    calculate_xfactor(-1)
except ValueError as error:
    print(error)
