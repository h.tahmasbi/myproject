# Cost of Raising Exceptions
from timeit import timeit
# raise exceptions if you really have to do!!!!
# with
code1 = """
def calculate_xfactor(age):
    if age <= 0:
        raise ValueError("Age cannot be 0 or less")
    return 10 / age
try:
    calculate_xfactor(-1)
except ValueError as error:
    pass
"""
# without
code2 = """
def calculate_xfactor(age):
    if age <= 0:
        return None 
    return 10 / age

xfactor = calculate_xfactor(-1)
if xfactor == None:
    pass
"""


print("first code1", timeit(code1, number=10000))
print("second code2", timeit(code2, number=10000))
