# Arrays: for large list of numbers
# use only when dealing a large sequence of numbers and you encounter performance problem 
from array import array

# python 3 typecode: https://docs.python.org/3/library/array.html
numbers = array("i", [1, 2, 3])
print(numbers[2])
print(numbers)
# similar to list we can use: append, pop, insert, access with index, so on ...
# unlike to list : objects are typed!!
# typecode: signed int -> only integer number
numbers[0] = 1.0  # error! because it is a float.
