# Dictionary
point = {"x": 1, "y": 2}
point = dict(x=1, y=2)
print(point["x"])
point["x"] = 10
point["z"] = 20
if "a" in point:
    print(point["a"])

print(point.get("a"))
print(point.get("a", 0))  # if there is no key "a" print 0

del point["x"]
print(point)

# Loop over dictionaries
for key in point:
    print(key, point[key])

# or # as tuple
for x in point.items():
    print(x)

# or
for key, value in point.items():
    print(key, value)

# Dictionary comprehensions
# values = []
# for x in range(5):
#     values.append(x * 2)
#
# syntax: [expression for item in items]
values = [x * 2 for x in range(5)]
print(values)
# as dictionary
values = {x: x * 2 for x in range(5)}
print(values)
# if we use parentheses we do not get tuple but generator
values = (x * 2 for x in range(5))
print(values)

# Generator Expressions, is iterable
# for a large data set and to not use much memory
from sys import getsizeof

values = (x * 2 for x in range(100000))
print("gen:", getsizeof(values))
values = [x * 2 for x in range(100000)]
print("list:", getsizeof(values))
# for x in values:
#    print(x)
# Note that a generator object has no len()
# print(len(values)) #error

# Unpacking Operator
# for list : *, for dictionary: ** 
numbers = [1, 2, 3]
print(*numbers)
print(1, 2, 3)
values = list(range(5))
values = [*range(5), *"Hello"]
print(values)
# combine two lists with unpacking
first = [1, 2]
second = [3]
values = [*first, *second]
print(values)
# 
# for dictionary: ** 
first = {"x": 1}
second = {"x": 10, "y": 2}
combined = {**first, **second, "z": 1}
print(combined)
