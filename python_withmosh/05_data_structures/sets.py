# Set : is an unordered collection of unique items
# collection of no duplicates
numbers = [1, 1, 2, 3, 4]
uniques = set(numbers)
print(uniques)
first = set(numbers)
second = {1, 5}
# second.add(5)
# second.remove(5)
# len(second)
print(first | second)
print(first & second)
print(first - second)
print(first ^ second)
# we can not access the items with index in set
# first[0] # error
# only using loop 
if 1 in first:
    print("yes")
