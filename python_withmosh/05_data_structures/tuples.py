# Tuples : can not be modified (add or remove items)
# when you do not want to accidentally change modify your data you use tuple
# All these are tuple
point = (1, 2)
point = 1,
point = 1, 2
point = ()
print(type(point))
# similar to lists: concatenate 
point = (1, 2) + (3, 4)
print(point)
# (1, 2, 3, 4)
# repeat
point = (1, 2) * 3
print(point)
# convert list to tuple
point = tuple([1, 2, 3])  # tuple(iterable)
print(point)
# unpack
point = (1, 2, 3)
print(point[0:2])
x, y, z = point
print(f"x: {x}, y: {y}, z: {z}")
if 3 in point:
    print("exists")
