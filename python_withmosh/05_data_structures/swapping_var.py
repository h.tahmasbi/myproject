# swapping variables
x = 10
y = 11

# instead of using another variable like z 
# z = x
# x = y
# y = z
# we can define multiple variables as a tuple:
x, y = y, x  # x, y = 11, 10 -> a tuple

print("x", x)
print("y", y)
