# Queues
# FIFO: First In First Out
# [1, 2, 3] -> [2, 3] -> [3] -> []
# if we remove an item of a list, all the items should be moved to the left.
# For a list with thousands + 1 items, a thousand items need to be moved in memory when one item is removed.
# using deque class
from collections import deque

queue = deque([])
queue.append(1)
queue.append(2)
queue.append(3)
queue.popleft()
print(queue)
if not queue:  # means True -> [] if False
    print("empty")
