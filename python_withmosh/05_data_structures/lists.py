# Lists
letters = ["a", "b", "c"]
matrix = [[0, 1], [2, 3]]
zeros = [0] * 5
combined = zeros + letters
numbers = list(range(20))
chars = list("Hello World")
print(len(chars))
# ---------------------------------------------------
# Accessing items
letters = ["a", "b", "c", "d"]
print(letters[-1])
letters[0] = "A"
print(letters)
print(letters[::2])

numbers = list(range(20))
print(numbers[::-1])
# ---------------------------------------------------
# List unpacking
numbers = [1, 2, 3, 4, 5, 6]
first, second, *other, last = numbers
print(first, last)
print(other)
# ---------------------------------------------------
# Looping over lists
letters = ["a", "b", "c"]
# for letter in enumerate(letters):
#    print(letter[0], letter[1])
for index, letter in enumerate(letters):
    print(index, letter)
# ---------------------------------------------------
# Adding and removing items 
letters = ["a", "b", "c"]
# Add
letters.append("d")
letters.insert(0, "-")

# Remove
letters.pop()  # remove from the end of list
letters.pop(0)  # remove an item at the given index
letters.remove("b")  # remove the first occurence of "b"
del letters[0:3]  # remove a range of objects
letters.clear()  # all the objects

print(letters)
# ---------------------------------------------------
# Finding items
letters = ["a", "b", "c"]
print(letters.count("d"))
if "d" in letters:
    print(letters.index("d"))
# ---------------------------------------------------
# Sorting lists
numbers = [3, 51, 2, 8, 6]
numbers.sort()
numbers.sort(reverse=True)
print(sorted(numbers, reverse=True))
print(numbers)
# more complicated list
items = [
    ("Product1", 10),
    ("Product2", 9),
    ("Product3", 12),
]


def sort_item(item):
    return item[1]


items.sort(key=sort_item)
print(items)
# ---------------------------------------------------
# Lambda functions
# Lambda functions are used when you need a function for a short period of time.
items.sort(key=lambda item: item[1])
# items.sort(key=lambda parameters:expression)
print(items)
# ---------------------------------------------------
# Map function
items = [
    ("Product1", 10),
    ("Product2", 9),
    ("Product3", 12),
]

prices = []
for item in items:
    prices.append(item[1])

print(prices)

# Map using lambda:
prices = list(map(lambda item: item[1], items))
print(prices)
# ---------------------------------------------------
# Filter function
# finding products with price >= 10
# using built-in function "filter"
filtered = list(filter(lambda item: item[1] >= 10, items))
print(filtered)
# ---------------------------------------------------
# List comprehensions instead of map and filter
# syntax: [expression for item in items]

prices = list(map(lambda item: item[1], items))
prices = [item[1] for item in items]
print(prices)

filtered = list(filter(lambda item: item[1] >= 10, items))
filtered = [item for item in items if item[1] >= 10]
print(filtered)

# ---------------------------------------------------
# Zip function
list1 = [1, 2, 3]
list2 = [10, 20, 30]

print(list(zip(list1, list2)))
# [(1, 10), (2, 20), (3, 30)]

print(list(zip("abc", list1, list2)))
# [('a', 1, 10), ('b', 2, 20), ('c', 3, 30)]
# ---------------------------------------------------
# Stacks
# LIFO: Last In - First Out
browsing_session = []
# use append to add an item on top of a stack
browsing_session.append(1)
browsing_session.append(2)
browsing_session.append(3)
print(browsing_session)
# use pop to remove an item on top of stack
last = browsing_session.pop()
print(last)
print(browsing_session)
# check if a stack is empty or not
if not browsing_session:
    print("disable")
# use [-1] to get an item on top of stack
print("redirect", browsing_session[-1])
# ---------------------------------------------------
