# Variables
# numbers (integer or float), boolian (True, False), string 
# Note: python is case sensitive

# Variable names
students_count = 1000
rating = 4.99
is_published = True
course_name = "Python Programming"

# -----------------------------------------
# escape sequences using \
# \"
# \'
# \\
# \n   : next line
course = "Python \"Programming"
print(course)

# -----------------------------------------
# formatted strings
first = "Hossein"
last = "Tahmasbi"
# full = f"{first} {last}"
full = f"{len(first)} {5 + 2}"

print(full)
#
# strip -> rm white spaces
course = " python  programming"
print(course.upper())
print(course.lower())
print(course.title())
print(course.rstrip())
print(course.lstrip())
print(course.find("pro"))
print(course.replace("p", "j"))
print("pro" in course)
print("swift" not in course)
# -----------------------------------------
# numbers

x = 1
x = 1.1
x = 1 + 2j  # a + bi

# operators: + - * / // %
print(10 / 3)
print(10 // 3)
print(10 % 3)  # Modulus
# augmented
# x = x + 3
# x += 3
import math

print(round(2.9))
print(abs(-2.9))

print(math.ceil(2.2))

# -----------------------------------------
# type conversion

x = input("x: ")
print(type(x))
y = int(x) + 1
print(f"x: {x}, y: {y}")
# bool(x), float(x), int(x)

# Falsy for boolian
# "" -> empty string
# 0
# None

# all the others True
print(bool(""))
print(bool(1))
print(bool("False"))
