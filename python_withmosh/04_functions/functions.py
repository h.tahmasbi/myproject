# functions
def greet():
    print("Hi there")
    print("Welcome aboard")


greet()


# --------------------------------------------
# Arguments
def greet(first_name, last_name):
    print(f"Hi {first_name} {last_name}")
    print("Welcome aboard")


greet("Hossein", "Tahmasbi")


# --------------------------------------------
# types of functions:
# 1. perform a task
# 2. return a value
# print in terminal
def greet(name):
    print(f"Hi {name}")


# write in file
def get_greeting(name):
    return f"Hi {name}"


message = get_greeting("Tahmasbi")
file = open("content", "w")
file.write(message)


# --------------------------------------------
# keyword arguments
def increment(number, by):
    return number + by


print(increment(2, by=1))


# --------------------------------------------
# default arguments
# all the optional parameters should come after the required parameters.
# def increment(number, by=1, another): #incorrect
# def increment(number, another, by=1): #correct
def increment(number, by=1):
    return number + by


print(increment(2))


# --------------------------------------------
# xargs *args
def multiply(*numbers):
    print(numbers)


multiply(2, 3, 4, 5)


# > (2, 3, 4, 5)  # a tuple
# list [1, 2, 3]; tuple (1, 2, 3) both of them are iterable but 
# tuple can not be modified (e.g. adding a new number).
def multiply(*numbers):
    total = 1
    for number in numbers:
        total *= number
    return total


print(multiply(2, 3, 4, 5))


# --------------------------------------------
# xxargs **args
# multiple keywords, Python will automatically package them as a dictionary
def save_user(**user):
    print(user)
    print(user["id"])
    print(user["name"])


save_user(id=1, name="John", age=22)

# > {'id': 1, 'name': 'John', 'age': 22} # dictionary
# --------------------------------------------
# scope
# local and global variable
# avoid defining global variables as much as possible
# for local variables, python interpreter will release the memory that allocated for this variable after execution
new_message = "d"  # global


def greet(name):
    # global new_message
    new_message = "a"  # local


def send_email(name):
    new_message = "b"


print(new_message)
# --------------------------------------------
